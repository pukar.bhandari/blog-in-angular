import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'limitCharacters'
})
export class LimitCharactersPipe implements PipeTransform {

  transform(value: any,lettersNum: number ): string {
    return value.slice(0,lettersNum);
  }
}
