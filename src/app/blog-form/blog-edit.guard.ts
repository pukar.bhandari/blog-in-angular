import {Injectable} from '@angular/core';
import {CanDeactivate} from '@angular/router';
import {BlogEditComponent} from "./blog-edit.component";

@Injectable({
    providedIn: 'root'
})
export class BlogEditGuard implements CanDeactivate<BlogEditComponent> {
    canDeactivate(component: BlogEditComponent) {
        if (component.blogForm.dirty) {
            const title = component.blogForm.get('title').value || 'New Blog';
            return confirm(`Navigate away and lose all changes to ${title}?`);
        }
        return true;
    }
}
