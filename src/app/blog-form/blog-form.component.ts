import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {BlogFormService} from "./blog-form.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-blog-form',
  templateUrl: './blog-form.component.html',
  styleUrls: ['./blog-form.component.css']
})

export class BlogFormComponent implements OnInit {
  blogForm: FormGroup;

  constructor(private fb: FormBuilder,private blogService: BlogFormService, private router: Router) { }

  ngOnInit() {
    this.blogForm = this.fb.group({
      title: ['',[Validators.required,Validators.minLength(5)]],
      authorName: ['',[Validators.required,Validators.minLength(3)]],
      content: ['',[Validators.required,Validators.minLength(10)]]
    })
  }
  
  save() {
    this.blogService.createBlog(this.blogForm.value) .subscribe(response =>{
      this.blogForm.reset();
      this.router.navigate(['/blogs']);
    });
  }
}
