import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {BlogFormService} from "./blog-form.service";

@Injectable()
export class BlogsResolver implements Resolve<any> {
    constructor(private blogService: BlogFormService) {
    }

    resolve() {
        return this.blogService.getBlogs();
    }
}

@Injectable()
export class BlogResolver implements Resolve<any> {

    constructor(private blogService: BlogFormService) {
    }

    resolve(route: ActivatedRouteSnapshot) {
        let id = +route.params['id'];
        return this.blogService.getBlog(id);
    }
}