import { TestBed } from '@angular/core/testing';

import { BlogFormService } from './blog-form.service';

describe('BlogFormService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BlogFormService = TestBed.get(BlogFormService);
    expect(service).toBeTruthy();
  });
});
