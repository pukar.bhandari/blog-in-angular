import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {BlogFormService} from "./blog-form.service";
import {ActivatedRoute, Router} from "@angular/router";
import {BlogTemplate} from "./blog-form";

function contentLength(min: number): ValidatorFn{
    return (c: AbstractControl): {[key: string]: boolean} | null => {
        if(c.value!== null && c.value.length<min){
            return {'range': true};
        }
        return null;
    }
}


@Component({
    selector: 'app-blog-edit',
    templateUrl: './blog-edit.component.html',
    styleUrls: ['./blog-edit.component.css']
})
export class BlogEditComponent implements OnInit {
    blog: BlogTemplate;
    blogForm: FormGroup;

    constructor(private fb: FormBuilder, private blogService: BlogFormService, private router: Router, private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.blogForm = this.fb.group({
            title: ['', [Validators.required, Validators.minLength(5)]],
            authorName: ['', [Validators.required, Validators.minLength(3)]],
            content: ['', [Validators.required,contentLength(150) ]],
            date: new Date()
        });
        // this.route.paramMap.subscribe(
        //     params => {
        //         const id = +params.get('id');
        //         if(id) {
        //             this.getBlog(id);
        //         }
        //     }
        // );
        if(this.route.snapshot.data.blog) {
            this.displayBlog(this.route.snapshot.data.blog);
        }
    }

    // getBlog(id: number): void {
    //     this.blogService.getBlog(id)
    //         .subscribe({
    //             next: (blog: BlogTemplate) => {
    //                 this.displayBlog(blog)
    //             },
    //         });
    // }

    displayBlog(blog: BlogTemplate): void {
        if (this.blogForm) {
            this.blogForm.reset();
        }
        this.blog = blog;
        this.blogForm.patchValue({
            title: this.blog.title,
            authorName: this.blog.authorName,
            content: this.blog.content,
            date: new Date()
        });
    }

    save(): void {
        if (this.router.url === '/blogs/add') {
            this.blogService.createBlog(this.blogForm.value).subscribe(response => {
                this.onSaveComplete();
            });
        } else {
            this.blogService.updateBlog(this.blogForm.value, this.blog.id).subscribe(response => {
                this.onSaveComplete();
            })
        }
    }

    onSaveComplete(): void {
        this.blogForm.reset();
        this.router.navigate(['/blogs']);
    }
}
