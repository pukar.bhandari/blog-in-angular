import { TestBed, async, inject } from '@angular/core/testing';

import { BlogEditGuard } from './blog-edit.guard';

describe('BlogEditGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BlogEditGuard]
    });
  });

  it('should ...', inject([BlogEditGuard], (guard: BlogEditGuard) => {
    expect(guard).toBeTruthy();
  }));
});
