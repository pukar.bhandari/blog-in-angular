import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {BlogTemplate} from "./blog-form";

@Injectable({
  providedIn: "root"
})

export class ApiService {

  constructor(private http: HttpClient) { }

  get(url: string): Observable<any>{
    return this.http.get(url);
  }

  post(url: string, blog: BlogTemplate): Observable<any>{
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.post(url,JSON.stringify(blog),{headers})
  }

  put(url: string, blog: BlogTemplate): Observable<any>{
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put(url, blog, { headers })
  }

  delete(url: string): Observable<any>{
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.delete(url, { headers })
  }
}
