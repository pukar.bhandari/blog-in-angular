export interface BlogTemplate {
    id: number,
    title: string,
    authorName: string,
    content: string,
    date: Object
}
