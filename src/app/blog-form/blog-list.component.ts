import {Component, OnInit} from '@angular/core';
import {BlogFormService} from "./blog-form.service";
import {BlogTemplate} from "./blog-form";
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";

@Component({
    selector: 'app-blog-list',
    templateUrl: './blog-list.component.html',
    styleUrls: ['./blog-list.component.css']
})

export class BlogListComponent implements OnInit{

    blogs: BlogTemplate[] = [];
    showList: boolean;

    constructor(private blogService: BlogFormService, private route: ActivatedRoute, private router: Router) {
    }

    ngOnInit(): void {
        this.showList = this.route.snapshot.children.length === 0;
        this.getBlogs();
        this.router.events.subscribe((event) =>{
            if (event instanceof NavigationEnd) {
                this.showList = event.url === '/blogs';
                if (this.showList){
                    this.getBlogs();
                }
            }
        })
    }

    private getBlogs(): void {
        // this.blogService.getBlogs()
        //     .subscribe(blogs => {
        //         this.blogs = blogs;
        //     });
        this.blogs = this.route.snapshot.data.blogs;
    }
}



