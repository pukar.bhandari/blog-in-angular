import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {BlogFormService} from "./blog-form.service";
import {BlogTemplate} from "./blog-form";

@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.css']
})

export class BlogDetailComponent implements OnInit {
    blog: BlogTemplate;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private blogService: BlogFormService) {
    }

    ngOnInit() {
        // this.route.paramMap.subscribe(
        //     params => {
        //         const id = +params.get('id');
        //         this.getBlog(id);
        //     }
        // );
        this.blog = this.route.snapshot.data.blog;
    }

    // getBlog(id: number) {
    //     this.blogService.getBlog(id).subscribe({
    //         next: blog => this.blog = blog
    //     });
    // }

    onBack(): void {
        this.router.navigate(['/blogs']);
    }

    onDelete(): void{
        if (this.blog.id) {
            if (confirm(`Really delete the blog: ${this.blog.title}?`)) {
                this.blogService.deleteBlog(this.blog.id)
                    .subscribe({
                        next: () => this.onBack()
                    });
            }
        } else {
            this.onBack();
        }
    }
}
