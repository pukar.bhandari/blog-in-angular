import { Injectable } from '@angular/core';
import {BlogTemplate} from "./blog-form";
import {Observable, of} from "rxjs";
import {tap} from "rxjs/operators";
import {ApiService} from "./api.service";

@Injectable({
  providedIn: 'root'
})

export class BlogFormService {
  private blogUrl = 'http://localhost:3000/posts';
  private blogs: BlogTemplate[];

  constructor(private API: ApiService) { }

  getBlogs(): Observable<BlogTemplate[]> {
    if(this.blogs) {
      return of(this.blogs);
    }
    return this.API.get(this.blogUrl)
        .pipe(
            tap(data => this.blogs = data)
        );
  }

  getBlog(id: number): Observable<BlogTemplate> {
    if(this.blogs){
      const foundItem = this.blogs.find(item => item.id===id);
      if(foundItem){
        return of(foundItem);
      }
    }
    const url = `${this.blogUrl}/${id}`;
    return this.API.get(url);
  }

  createBlog(blog: BlogTemplate): Observable<any>{
    return this.API.post(this.blogUrl,blog)
        .pipe(
            tap(data => this.blogs.push(data))
        );
  }

  updateBlog(blog: BlogTemplate,id): Observable<any> {
    const url = `${this.blogUrl}/${id}`;
    return this.API.put(url, blog)
        .pipe(
            tap(data => this.blogs[id-1]=data)
        );
  }

  deleteBlog(id: number): Observable<any> {
    const url = `${this.blogUrl}/${id}`;
    return this.API.delete(url)
        .pipe(
            tap(data => {
              const foundIndex = this.blogs.findIndex(item => item.id === id);
              if (foundIndex > -1) {
                this.blogs.splice(foundIndex, 1);
              }
            }),
        );
  }
}
