import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  // templateUrl: 'app.component.html'
  template: `
    <nav class='navbar navbar-expand navbar-light bg-light'>
      <a class='navbar brand'>{{title}}</a>
      <ul class='nav nav-pills'>
        <li><a class='nav-link' [routerLink]="['/welcome']">Home</a></li>
        <li><a class='nav-link' [routerLink]="['/blogs']">Blogs</a></li>
        <li><a class='nav-link' [routerLink]="['/add']">Add Blogs</a></li>
       </ul>
      </nav>
      <div class="container">
        <router-outlet></router-outlet>
      </div>
  `
})
export class AppComponent {
  title = 'Trekking blogs'
}
