import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {RouterModule} from "@angular/router";
import { BlogListComponent } from './blog-form/blog-list.component';
import { BlogDetailComponent } from './blog-form/blog-detail.component';
import { BlogEditComponent } from './blog-form/blog-edit.component';
import {BlogEditGuard} from "./blog-form/blog-edit.guard";
import {BlogResolver, BlogsResolver} from "./blog-form/blog-form.resolver";
import { LimitCharactersPipe } from './shared/limit-characters.pipe';
import { DateAgoPipe } from './shared/date-ago.pipe';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    BlogListComponent,
    BlogDetailComponent,
    BlogEditComponent,
    LimitCharactersPipe,
    DateAgoPipe,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
      HttpClientModule,
      RouterModule.forRoot([
        { path: 'welcome',component: WelcomeComponent},
        // {
        //   path: 'blogs/edit/:id',
        //   canDeactivate: [BlogEditGuard],
        //   component: BlogEditComponent,
        //
        // },
        // {path:'blogs',component: BlogListComponent},
        {path:'blogs',component: BlogListComponent,
          resolve: {
            blogs: BlogsResolver
          },
          children:[
            {
              path:'add',
              component: BlogEditComponent,
              canDeactivate: [BlogEditGuard]
            },
            {
              path:':id',
              component: BlogDetailComponent,
              resolve: {
                blog: BlogResolver
              }
            },
            {
              path:'edit/:id',
              component: BlogEditComponent,
              canDeactivate: [BlogEditGuard],
              resolve: {
                blog: BlogResolver
              }
            }
          ]
        },
        // {path: 'blogs/:id', component: BlogDetailComponent} ,
        {path: '',redirectTo: 'welcome',pathMatch: 'full'},
        {path: '**',redirectTo: 'welcome',pathMatch: 'full'}
      ])
  ],
  providers: [BlogsResolver,BlogResolver],
  bootstrap: [AppComponent]
})
export class AppModule { }
